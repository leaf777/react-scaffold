// yxconnect(mapState,mapDispatch)
export default (mapStateToProps,mapDispatchToProps)=> {
  return WrappedComponent=> {
    return props=>{
      return (
        <WrappedComponent {...props}/>
      )
    }
  }
}