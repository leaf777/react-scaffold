import axios from 'axios'

// const baseURL = '/api/v1'

const baseURL = 'http://localhost:9000'
// const baseURL = 'https://c.y.qq.com'

// 创建axios实例
const instance = axios.create({
  baseURL,
  timeout: 7000,
  headers: {}
})

// 封装请求拦截器
instance.interceptors.request.use(function (config) {
  // 加token
  //给请求的头部加上token，所以可以在后端进行token的验证
  config.headers.Authorization = localStorage.getItem('token')
  return config
}, function (error) {
  return Promise.reject(error)
})

// 封装响应拦截器
instance.interceptors.response.use(function (response):any {
  let res = null
  // console.log('res ', response)
  // 对后端返回的数据，做统一拦截处理
  if(response.status===200) {
    console.log(response.data,'拦截')
    res = response.data.data
  }else{
    alert('网络异常，稍后再试')
  }
  return res
}, function (error) {
  return Promise.reject(error)
})

export default instance