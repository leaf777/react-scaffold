import React,{ useEffect } from 'react'

import { Layout,Login } from '@/components/index'
import { Route, useHistory } from 'react-router-dom'
import { useAppSelector,useAppDispatch } from '@/hooks'
import { getInfo } from '@/store/actions/index'
export default () => {
       const History = useHistory()
       const token = useAppSelector(({test}) => test.token)
       const dispatch=useAppDispatch()
       const user = useAppSelector(({test}) => test.user)
      useEffect(()=>{
        // console.log(getInfo(token),'getInfo(token).Promise.PromiseResult')
        if(token) dispatch(getInfo(token))
      },[token])
      useEffect(()=>{
        console.log(token,'token')
        console.log('user',user)
        if(!token) History.replace('/login')
      }, [token])
       return (
         user.role
         ?
         <Route path="/" component={Layout}/>
         :
         <Route path="/login" component={Login}/>
       )
     }