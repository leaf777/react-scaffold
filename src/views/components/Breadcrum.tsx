import { Breadcrumb,Alert } from 'antd'
import { HashRouter as Router, Route, Switch, Link, withRouter } from 'react-router-dom';

export default (props)=> {
  // 可以试试这种面包屑
 /* const routes = [
    {
      path: '/roleManage',
      breadcrumbName: '角色管理',
      children: [
        {
          path: '/roleManage/AddRole',
          breadcrumbName: '增加角色',
        },
        {
          path: '/roleManage/DeleteRole',
          breadcrumbName: '删除角色',
        }
      ],
    },
    {
      path: '/staffManage',
      breadcrumbName: '员工管理',
      children: [
        {
          path: '/staffManage/addStaff',
          breadcrumbName: '增加员工',
        },
      ]
    },
  ]
  const itemRender =(route, params, routes, paths)=> {
    console.log(routes,'000000')
    const last = routes.indexOf(route) === routes.length - 1;
    console.log(last,'9999999')
    console.log(paths,'888888')
    return last ? (
      <span>{route.breadcrumbName}</span>
    ) : (
      <Link to={paths.join('/')}>{route.breadcrumbName}</Link>
    );
  
  return(
    <Breadcrumb itemRender={itemRender} routes={routes} />
  )*/
  const breadcrumbNameMap = {
    '/map':'城市地图',
    '/txmap':'腾讯地图',
    '/stepForm':'分布表单',
    '/communication':'通信',
    '/component两种': '组件',
    '/Combine':'高级组件',
    '/condition':'Condition',
    '/Card':'权益卡',
    '/AddTitle':'新增文章',
    '/ChangeTitle':'修改文章',
    '/SelectTitle':'查看文章',
    '/SelectUser':'查看用户',
    '/roleManage': '角色管理',
    '/staffManage': '员工管理',
    '/staffManage/addStaff': '增加员工',
    '/roleManage/AddRole': '增加角色',
    '/roleManage/DeleteRole': '删除角色',
    '/Card/':'权益卡管理',
    '/Card/CardRecord':'领卡记录',
    '/Card/FadeRecord':'退卡记录',
    '/Card/AddCard':'添加权限',
    '/RoleCard/TemplateImage':'虚假界面',
    '/RoleCard':'图片处理'
  };
  const Home = withRouter(props => {
    const { location } = props;
    const pathSnippets = location.pathname.split('/').filter(i => i);
    const extraBreadcrumbItems = pathSnippets.map((_, index) => {
      const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
      return (
        <Breadcrumb.Item key={url}>
          {console.log(url,'url')}{
          console.log(breadcrumbNameMap[url],'breadcrumbNameMap[url]')}
          <Link to={url}>{breadcrumbNameMap[url]}</Link>
        </Breadcrumb.Item>
      );
    });
    const breadcrumbItems = [
      <Breadcrumb.Item key="home">
        <Link to="/">Home</Link>
      </Breadcrumb.Item>,
    ].concat(extraBreadcrumbItems);
    return(
      <Breadcrumb>{breadcrumbItems}</Breadcrumb>
    )
  });
  return(
    <Home />
  )

}