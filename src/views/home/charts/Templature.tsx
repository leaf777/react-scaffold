import React from 'react'
import ReactDOM from 'react-dom';
import { Chart, LineAdvance} from 'bizcharts';
export default (props)=>{
	console.log(props)
  return(
    <div>
      <Chart padding={[10, 20, 50, 40]} autoFit height={300} data={props.data} >
		<LineAdvance
			shape="smooth"
			point
			area
			position="month*temperature"
			color="city"
      interactions={['continuous-filter']}
		/>
	
	</Chart>

    </div>
  )

}