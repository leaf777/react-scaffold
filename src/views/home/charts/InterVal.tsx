import { Chart, Interval, Tooltip } from 'bizcharts';

export default (props)=>{
  
    return (
      <Chart height={300} autoFit data={props.data} >
      <Interval position="year*sales" />
      <Tooltip shared/>
      </Chart>
    )
  
}