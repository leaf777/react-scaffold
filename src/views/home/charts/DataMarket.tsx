import React, { useState, useEffect } from "react";
import { Chart, Point, Line, Annotation } from "bizcharts";

export default (props) =>{
  const [data, setData] = useState();
  const scale = {
    rate: {
      nice: true,
    },
  };
  const {dataMarkerCfg1,dataMarkerCfg2,dataMarkerCfg3,dataMarkerCfg4,dataMarkerCfg5,dataRegionCfg}=props.num
  useEffect(() => {
    fetch("https://gw.alipayobjects.com/os/antvdemo/assets/data/income.json")
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setData(data);
      });
  }, []);

  return (
    <Chart height={400} data={data} autoFit scale={scale}>
      <Line position="time*rate" />

      <Point
        position="height*weight"
        color="gender"
        shape={["gender", ["circle", "square"]]}
        style={{
          fillOpacity: 0.85,
        }}
      />
      <Annotation.DataMarker {...dataMarkerCfg1} />
      <Annotation.DataMarker {...dataMarkerCfg2} />
      <Annotation.DataMarker {...dataMarkerCfg3} />
      <Annotation.DataMarker {...dataMarkerCfg4} />
      <Annotation.DataMarker {...dataMarkerCfg5} /> 
     <Annotation.DataRegion {...dataRegionCfg} />
    </Chart>
  )
}