import React, { useEffect } from 'react'
import { useMemo } from 'react'
import Templature from './charts/Templature'
import RadarChart from './charts/RadarChart'
import BizCharts from './charts/BizCharts'
import DataMarket from './charts/DataMarket'
import InterVal from './charts/InterVal'
import { useAppDispatch,useAppSelector } from '@/hooks'
import { getChartInfo } from '@/store/reducers/chart'

export default () => {
  const charInfo:any = useAppSelector(state=>state.chart.chart)
  const dispatch = useAppDispatch()
  const tempList = useMemo(()=>{
    dispatch(getChartInfo({}))
  },[])
  const {cityTemp,radarData,bizChart,dataMark,interval}=useMemo(()=>{
    let cityTem:any=[]
    let radarData:any=[]
    let ttt:any=[]
    let dataMark=[]
    let inter=[]
    let Mon=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    charInfo.cityTemplature && charInfo.cityTemplature.map(ele1=>{
      ele1.templature && ele1.templature.map((ele,idx)=>{
        cityTem.push({
          month: Mon[idx],
          city: ele1.city,
          temperature: ele
        })
      })
    })
    charInfo.radarData && charInfo.radarData.map(ele=>{
      radarData.push(ele)
    })
    if(charInfo.bizChart){
      var s=0
      charInfo.bizChart['x'].map(ele2=>{
        ttt[s]={}
        s++
      })
      for(let ele in charInfo.bizChart){  
        var s=0; //3
        charInfo.bizChart[ele].map(ele2=>{   //99
          switch(ele){
            case 'x':ttt[s]['x']=ele2
            break;
            case 'value':ttt[s]['value']=ele2
            break;
            case 'category':ttt[s]['category']=ele2
            break;
            default:
          }
          s++;
        }) 
      }
    }
    if(charInfo.dataMarket){
      dataMark=JSON.parse(JSON.stringify(charInfo.dataMarket))
    }
    if(charInfo.Interval){
      inter=JSON.parse(JSON.stringify(charInfo.Interval))
    }
    return {cityTemp:cityTem,radarData:radarData,bizChart:ttt,dataMark: dataMark,interval:inter}
  },[charInfo])
  return (
    <div>
      <h1>我是打工人editor的首页</h1>
      <Templature data={cityTemp}/>
      <RadarChart data={radarData}/>
      <BizCharts data={bizChart}/>
      <DataMarket num={dataMark}/>
      <InterVal data={interval} />
    </div>
  )
}