import { useAppSelector } from "@/hooks";
import React, { useMemo } from 'react';

import HomeAdmin from "@/views/home/HomeAdmin";
import HomeEditor from "@/views/home/HomeEditor";


export default()=>{
  const u=useAppSelector(({test})=>test.user)
  const dash = useMemo(()=>{
    let result:any =null
    switch(u.role[0]){
      case 'admin':
        result = <HomeAdmin />
      break;
      case 'editor':
        result=<HomeEditor />
      break;
      default:
    }
    return result
  },[u])
  return (
    <div>
      { dash }
    </div>
  )

}


