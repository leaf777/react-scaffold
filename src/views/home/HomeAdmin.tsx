import React,{useMemo} from 'react'
import AreaChart from '@/views/home/echarts/AreaCharts'
import PieChart from './echarts/PieChart'
import PloarChart from './echarts/PloarChart'
import Negative from './echarts/Negative'
import Cateoption from './echarts/Cateoption'
import { useAppSelector,useAppDispatch } from '@/hooks'
import { getEchartInfo } from '@/store/reducers/chart'
export default () => {
  const dispatch=useAppDispatch()
  const echarInfo:any = useAppSelector(state=>state.chart.echart)
  const tempList = useMemo(()=>{
    dispatch(getEchartInfo({}))
  },[])
  const {firstop,pieoption,ploaroption,negaoption,cateoption}=useMemo(()=>{
    let firstop={}
    let pieoption={}
    let ploaroption={}
    let negaoption={}
    let cateoption={}
    if(echarInfo.firstoption)
      firstop=echarInfo.firstoption
    if(echarInfo.pieoption){
      pieoption=echarInfo.pieoption
    }
    if(echarInfo.ploaroption){
      ploaroption=echarInfo.ploaroption
    }
    if(echarInfo.negaoption){
      negaoption=echarInfo.negaoption
    }
    if(echarInfo.cateoption){
      cateoption=echarInfo.cateoption
    }
    return {firstop,pieoption,ploaroption,negaoption,cateoption}
  },[echarInfo])
  return (
    <div>
      <h1>我是管理员admin的首页</h1>
      <AreaChart data={firstop}/>
      <PieChart data={pieoption} />
      <PloarChart data={ploaroption} />
      <Negative data={negaoption} />
      <Cateoption data={cateoption} />

    </div>
  )
}