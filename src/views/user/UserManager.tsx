import React from 'react'
import { Breadcrumb ,Table, Input,Row, Col,Button,Modal,Select,Space} from 'antd';
import { HomeOutlined, UserOutlined,PlusOutlined } from '@ant-design/icons';
import { useAppDispatch ,useAppSelector}from '@/hooks'
import { useState,useEffect } from 'react';
import { selectUser,addUser } from '@/store/reducers/user'


export default ()=> {
  // 获取当前路由
  const { Search } = Input;
  const { Option } = Select

  const dispatch=useAppDispatch()
  const total = useAppSelector(({user})=>user.total)
  const list = useAppSelector(({user})=>user.list)
  const [name, setName] = useState('')
  const [page,setPage] = useState(1)
  const [user,setUser] = useState({role:'',username:''})
  // modal
  const [visible, setVisible] = React.useState(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [modalText, setModalText] = React.useState('Content of the modal')

  useEffect(()=>{
    dispatch(selectUser({name,page}))
  },[name,page])
  
  const onSearch = value => setName(value);
  const showModal = () => {
    setVisible(true);
  };

  const handleOk = () => {
    setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);
    // 用户调接口
    dispatch(addUser(user)).then(()=>{
      setVisible(false);
      setConfirmLoading(false);
    })

  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };

  const columns = [
    {
      title: '用户名',
      dataIndex: 'username',
      key: 'username',
      render: text => <a>{text}</a>,
    },
    {
      title: '权限',
      dataIndex: 'role_name',
      key: 'role_name',
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'role',
      render: s => (s?'正常':'离职')
    },
    {
      title: '操作',
      dataIndex: 'status',
      key: 'role',
      render: (s, record) => (
        <Space size="middle">
          <a >禁用 {record.name}</a>
          <a >启用</a>
        </Space>
      ),
    },
  ];
  const work= [
    {id:1,roleName:'系统管理员',role:'admin'},
    {id:2,roleName:'运营编辑',role:'editor'},
    {id:3,roleName:'超级管理员',role:'manager'},
  ]
  
  return(
    <div className='yx-user-manager'>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item href="">
          <UserOutlined />
          <span>查看用户</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item></Breadcrumb.Item>
      </Breadcrumb>
      <Row style={{padding:"10px 0"}}>
        <Col span={8}>
          <Search
            placeholder="请输入用户名"
            allowClear
            enterButton="Search"
            size="large"
            onSearch={onSearch}   
          />
        </Col>
        <Col span={12}></Col>
        <Col span={2}>
          <Button 
            type="primary" 
            icon={<PlusOutlined />} 
            size='large'
            onClick={showModal}  
          >
            添加管理员
          </Button>
        </Col>
      </Row>
      <Table 
        columns={columns} 
        dataSource={list} 
        rowKey='_id' 
        pagination={{
          current:page,
          total: total,
          pageSizeOptions:['2', '5', '7', '10'],
          showQuickJumper:true,
          showTotal: (total,range)=>{
            return <div>第{range[0]}-{range[1]}条/总共{total}条</div>
          },
          onChange: page=>setPage(page)
        }}
      />
      {/*  */}
      <Modal
        title="添加用户"
        visible={visible}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
         <div>
          <span>用户名：</span>
          <Input
            value={user.username}
            onChange={e=>setUser({...user, username:e.target.value})}
          />
        </div>
        <div>
          <span>选择身份：</span>
          <Select
            value={user.role}
            onChange={val=>setUser({...user, role: val})}
          >
          {
            work.map(ele=>(
              <Option key={ele.id} value={ele.role}>{ele.roleName}</Option>
            ))
          }
          </Select>
        </div>
      </Modal>
    </div>
  )

}
