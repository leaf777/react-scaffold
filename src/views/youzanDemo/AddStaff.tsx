
// import Breadcrum from "../components/Breadcrum"
import React ,{useEffect, useState} from 'react'
import { Form, Input, InputNumber, Button,Select,Descriptions ,Tree  } from 'antd';
import dynRoutes,{ constRouter } from '@/views'
import { ConsoleSqlOutlined,DownOutlined } from '@ant-design/icons'
import { useAppDispatch ,useAppSelector}from '@/hooks'
import {selectText} from '@/store/reducers/myroute'
export default ()=> {
  const dispatch = useAppDispatch()
  const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 16 },
  };
  const validateMessages = {
    required: '${label} is required!',
    types: {
      number: '${label} is not a valid number!',
    },
  };
  const onFinish = (values: any) => {
    console.log(values);
  };
  const onChange=(e)=>{
    console.log(e,'选择的是')
  }
  const { Option } = Select;
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select style={{ width: 70 }}>
        <Option value="86">+86</Option>
        <Option value="87">+87</Option>
      </Select>
    </Form.Item>
  );
  const info=useAppSelector(({myroute})=>myroute.myroute)
  let [routText,setRoutText]=useState([])
  let l:any=''
  useEffect(()=>{
    dispatch(selectText())
  },[])
  useEffect(()=>{
    if(info) setRoutText(info)
  },[info])
  console.log('----', routText)
  const onSelect = (selectedKeys, info) => {
    console.log('selected', selectedKeys, info);
  };
  return(
    <div>
      <div style={{width:'60%',margin:'20px auto'}}>
          <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
          <Form.Item
            name="phone"
            label="员工有赞账户"
            rules={[{ required: true, message: 'Please input your phone number!' }]}
          >
            <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
          </Form.Item>
          <Form.Item name={['user', 'name']} label="员工姓名" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name={['user', 'phone']} label="员工工号" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name={['user', 'role']} label="员工权限">
            <Select defaultValue="admin" style={{ width: 120 }} onChange={onChange}>
              <Option value="admin">admin</Option>
              <Option value="editor">editor</Option>
              <Option value="manager">manager</Option>
            </Select>
          </Form.Item>
          <Form.Item name={['user', 'roleDetail']} label="权限">
          <Tree
            showLine
            switcherIcon={<DownOutlined />}
            defaultExpandedKeys={['0-0-0']}
            onSelect={onSelect}
            treeData={routText}
          />

          </Form.Item>
          <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>

    </div>
  )
}