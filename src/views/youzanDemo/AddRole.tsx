import React, { useState, useEffect } from 'react'
import { Form, Input, Button, message,Tree } from 'antd';
import { useAppDispatch ,useAppSelector}from '@/hooks'
import {selectText} from '@/store/reducers/myroute'
export default () => {
  const dispatch = useAppDispatch()
  useEffect(()=>{
    dispatch(selectText())
  },[])
    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };
    const tailLayout = {
      wrapperCol: { offset: 8, span: 16 },
    };
    const [checkedKeys, setCheckedKeys] = useState<React.Key[]>(['0-0-0']);
    const [form] = Form.useForm();
    const onFinish = (values: any) => {
      values['checkedKeys'] = checkedKeys
      console.log(checkedKeys)
      if(!values.checkedKeys.length)message.error('请至少选择一项权限')
      console.log(values);
    };
    const onCheck = (checkedKeysValue,e) => {
      // console.log('onCheck', checkedKeysValue);
      // setCheckedKeys(checkedKeysValue);
      // localStorage.setItem('gn',checkedKeysValue)
      console.log(e.halfCheckedKeys,'98888888888')
      console.log('onCheck', checkedKeysValue.concat(e.halfCheckedKeys));
      setCheckedKeys(checkedKeysValue);
      // onChange(arr.concat(e.halfCheckedKeys))

    };
    const onReset = () => {
      form.resetFields();
    };
  
    const onFill = () => {
      form.setFieldsValue({
        note: 'Hello world!',
        gender: 'male',
      });
    };
    const onSelect = (selectedKeys, info) => {
      console.log('selected', selectedKeys, info);
    };
  const info=useAppSelector(({myroute})=>myroute.myroute)
  let [routText,setRoutText]=useState([])
  useEffect(()=>{
    if(info) setRoutText(info)
  },[info])
  return(
    <div>
    <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
      <Form.Item name="角色名称" label="角色名称" rules={[{ required: true }]} >
        <Input />
      </Form.Item>
      <Form.Item name="角色介绍" label="角色介绍">
        <Input.TextArea />
      </Form.Item>
      <Form.Item label="权限" rules={[{ required: true }]}>
        <Tree
          checkable
          onSelect={onSelect}
          onCheck={onCheck}
          treeData={routText}
        />
      </Form.Item>
      <Form.Item
        noStyle
        shouldUpdate={(prevValues, currentValues) => prevValues.gender !== currentValues.gender}
      >
        {({ getFieldValue }) =>
          getFieldValue('gender') === 'other' ? (
            <Form.Item name="customizeGender" label="Customize Gender" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
          ) : null
        }
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
         保存
        </Button>
        <Button htmlType="button" onClick={onReset}>
         重置
        </Button>
        <Button type="link" htmlType="button" onClick={onFill}>
          Fill form
        </Button>
      </Form.Item>
    </Form>
    </div>
  )
}
