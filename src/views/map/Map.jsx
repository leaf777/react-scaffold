import React, { useEffect, useState } from 'react'
import { Button, Input } from 'antd'
import styleJson from '../bmap/styleJson2'
import './style.scss'
import logo from '@/utils/img'
const BMapGL = window.BMapGL
let map
export default () => {

  const [pos, setPos] = useState({
    lat: '',
    lng: '',
    name: '',
    shop:''
  })

  useEffect(()=>{
    map = new BMapGL.Map("map")
    var point = new BMapGL.Point(116.404, 39.915)
    map.centerAndZoom(point, 15)
    map.centerAndZoom('深圳市西部硅谷', 12)
    // 功能：自定义地图样式
    map.setMapStyleV2({styleJson})

    map.addControl(new BMapGL.ScaleControl()) // 添加比例尺控件
    map.addControl(new BMapGL.ZoomControl()) // 添加缩放控件
    map.addControl(new BMapGL.CityListControl()) // 添加城市列表控件
  
    map.setZoom(17)
    map.enableScrollWheelZoom(true)
  }, [])
  useEffect(() => {
    // 单击事件
    map.addEventListener('click', function (e) {
      var pt = e.latlng;
      var geoc = new BMapGL.Geocoder();
      setPos({...pos, lat: e.latlng.lat, lng: e.latlng.lng})
      geoc.getLocation(pt, function(rs){
        var addComp = rs.addressComponents;
        $('#result_l').text(addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street + ", " + addComp.streetNumber);
    })
    });
  }, [])
  // useEffect(() => {
  //   // 创建自定义图标
  //   const myIcon = new BMapGL.Icon(logo.baby, new BMapGL.Size(26, 26));
  //   // 创建Marker标注，使用自定义图标
  //   if(point.lat!==''){
  //     const pt = new BMapGL.Point(point.lng, point.lat);
  //     const marker = new BMapGL.Marker(pt, {
  //         icon: myIcon
  //     });
  //     // 将标注添加到地图
  //     map.addOverlay(marker);
  //   }
    
  // }, [pos])

  useEffect(()=>{
    // 清楚全部障碍物
    map.clearOverlays();
    // 功能：添加Marker或覆盖物
    var marker = new BMapGL.Marker(pos);        // 创建标注
    map.addOverlay(marker)
  }, [pos])
  const addRouteType=()=>{
    map.setTrafficOn(); // 叠加路况图层
  }
  const removeRouteType=()=> {
    map.setTrafficOff(); // 关闭路况图层
  }
  const search = () => {
  var myGeo = new BMapGL.Geocoder();
  // 将地址解析结果显示在地图上，并调整地图视野
  myGeo.getPoint($('#result_l').text, function(point){
      if(point){
          map.centerAndZoom(point, 13);
          map.addOverlay(new BMapGL.Marker(point, {title:$('#result_l').text}))
      }else{
          alert('您选择的地址没有解析到结果！');
      }
  }, '深圳')
}
  return (
    <div className='qf-home-admin'>
      <div className='map' id="map"></div>
      <div id='result'>
        点击展示详细的地址：
        <div id='result_l' onClick={search}></div>
      </div>
      <ul className="btn-wrap" style={{zIndex:99}}>
        <li className = "btn" onClick = {addRouteType}>叠加路况</li>
        <li className = "btn" onClick = {removeRouteType}>删除路况</li>
      </ul>
      <div className='form'>
        <Input type="text" value={pos.lat} disabled /><br/>
        <Input type="text" value={pos.lng} disabled /><br/>
        <Input
          type="text"
          value={pos.shop}
          onChange={e=>setPos({...point, name:e.target.value})}
        /><br/>
        <Button type='primary' size='small'>添加店铺</Button>
      </div>
    </div>
  )
}