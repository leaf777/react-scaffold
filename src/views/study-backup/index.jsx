import React from 'react'

import PanelA from './components/PlanA'


export default () => {
  return (
    <div className='qf-study-redux'>
      <div>
        <PanelA />
      </div>
    </div>
  )
}