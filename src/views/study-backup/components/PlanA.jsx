import React from 'react'
import { connect } from 'react-redux'
import { TEST_MSG_UPDATE } from '../../../store/type'
import { updateMsg } from '../../../store/actions'

// 语法：connect(mapStateToProps,mapDispatchToProps)(WrappedComponent)

//store形参，可以改
//如果这里不把test结构出来的话，应该需要store.test
function mapStateToProps(store) {
  return {
    msg:store.test.msg
  }
}

function mapDispatchToProps(dispatch){
  return {
    //行为的封装
    setMsg(payload) {
      var action = { type:TEST_MSG_UPDATE,payload }
      console.log(action,'11111')
      dispatch(action)
    }
  }
}

// 装饰器
// @connect(mapStateToProps,mapDispatchToProps)
class StudyRedux extends React.Component{
  updateMsg(){
    // 现在角色是视图，要修改的数据在redux中
    console.log(this.props,'8888')
    this.props.setMsg('Gp6')
  }
  render(){
    console.log('props',this.props)
    return (
      <div>
        <h1>使用Redux</h1>
        <h1>{this.props.msg}</h1>
        <button onClick={()=>this.updateMsg()}>修改数据</button>
      </div>
    )
  }
}
// 如果不支持装饰器
export default connect(mapStateToProps,mapDispatchToProps)(StudyRedux)