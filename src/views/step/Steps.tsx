import React ,{useState,useEffect} from 'react'
import { Steps, Button, message,Form, Input, InputNumber,Select,Alert,Descriptions,Result } from 'antd';
import { useAppDispatch ,useAppSelector}from '@/hooks'
import {addOrder,selectOrder,changeOrder} from '@/store/reducers/step'
import './style.scss'

const { Step } = Steps;
const { Option } = Select;

const layout = {
  labelCol: { span: 24 },
  wrapperCol: { span: 14 },
};
const validateMessages = {
  required: '${label} is required!'
};
export default  () => {
  const dispatch = useAppDispatch()
  const orderId = useAppSelector(({step})=>step.orderId)
  const [current, setCurrent] = useState(0);
  const [password,setPassword] = useState('')
  const monOrder:any = useAppSelector(({step})=>step.moneyOrder)
  const [moneyOrder,setMoneyOrder] =useState({
    paymentId:'',paymentMethod:'',account:'',name:'',money:''
  })
  useEffect(()=>{
    const orderid=localStorage.getItem('orderId')
    if(orderid){
      setCurrent(1);
      dispatch(selectOrder({orderId:orderid}))
    }
  },[])
  useEffect(()=>{
    if(monOrder){
      let {paymentId,paymentMethod,account,name,money} = monOrder
      setMoneyOrder({paymentId,paymentMethod,account,name,money})
    }
  },[monOrder])
  // 第一步完成的表单
  const onFinish = () => {
    next()
    dispatch(addOrder(moneyOrder))
  };
  const content1=(
    <Form 
      {...layout} 
      name="nest-messages" 
      onFinish={onFinish} 
      validateMessages={validateMessages} 
      style={{width:'60%', margin:'0 auto'}}
    >
      <Form.Item 
        label="付款账户" 
        name={['moneyOrd', 'paymentId']} 
        rules={[{ required: true }]}
      >
        <Select
          allowClear
          placeholder="请选择付款账户"
          onChange={(value:any)=>setMoneyOrder({...moneyOrder,'paymentId':value})}
        >
          <Option value="1552572773">1552572773</Option>
        </Select>
      </Form.Item>
      <Form.Item label="收款账户">
        <Input.Group compact>
          <Form.Item 
            name={['moneyOrd', 'paymentMethod']} 
          >
            <Select
              allowClear
              placeholder="请选择"
              onChange={(value:any)=>setMoneyOrder({...moneyOrder,'paymentMethod':value})}
              style={{width:'150px',marginRight:'20px'}}
            >
              <Option value="银行账户">银行账户</Option>
              <Option value="支付宝">支付宝</Option>
            </Select>
          </Form.Item>
          <Form.Item  name={['moneyOrd', 'account']}>
            <Input 
              allowClear 
              placeholder="请输入账户" 
              style={{width:'200px'}}
              onChange={(e:any)=>setMoneyOrder({...moneyOrder,'account':e.target.value})}
            />
          </Form.Item>
        </Input.Group>
      </Form.Item>
      <Form.Item 
        name={['moneyOrd', 'name']} 
        label="收款人姓名" 
        rules={[{ required: true }]}
      >
        <Input 
          onChange={(e:any)=>setMoneyOrder({...moneyOrder,'name':e.target.value})}
        />
      </Form.Item>
      <Form.Item 
        label="转账金额"
        name={['moneyOrd', 'money']} 
        rules={[{ required: true }]}
      >
        <Input 
           onChange={(e:any)=>setMoneyOrder({...moneyOrder,'money':e.target.value})}
        />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Next
        </Button>
      </Form.Item>
    </Form>
  )
  const onFinish2 = (values: any) => {
    setPassword(values.password)
    const orderid=localStorage.getItem('orderId')
    dispatch(changeOrder({orderId:orderid}))
    next()
    dispatch(selectOrder({orderId:orderid}))
    // 要判断密码是否正确，但是我不想判断
    localStorage.removeItem('orderId')
  };
  const onPrevious = ()=>{
    prev()
  }
  const content2=(
    <div className='secontForm'>
      <Alert 
        showIcon 
        closable
        type="info" 
        message="确认转账后，资金将直接打入对方账户，无法退回" 
      />
      <Descriptions
        bordered
        column={1}
        labelStyle={{width:'200px',marginBottom:'20px'}}
      >
        {
          Object.keys(moneyOrder).map((ele)=>{
            return (
            <Descriptions.Item label={ele} key={ele}>
              {moneyOrder[ele]}
            </Descriptions.Item>
            )
          })
        }
      </Descriptions>
      <hr style={{color:'#fafafa'}}/>
      <Form
        name="basic"
        labelCol={{ span: 24 }}
        wrapperCol={{ span: 14 }}
        onFinish={onFinish2}
      >
        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item>
        <Button onClick={onPrevious} style={{marginRight:'20px'}}>
          上一步
        </Button>
        <Button type="primary" htmlType="submit">
          下一步
        </Button>
      </Form.Item>
      </Form>
    </div>
  )
  const content3=(
    <div 
      className='thirdForm'
      style={{marginTop:'-100px'}}
    >
      <Result
        status="success"
        title="操作成功"
        subTitle="预计两小时到账"
        extra={[
          <Button type="primary" key="again">
            再转一笔
          </Button>,
          <Button key="searchPay">查看账单</Button>,
        ]}
      /> 
      <Descriptions 
        column={1} 
        className='order'
        labelStyle={{marginLeft:'20px'}}
      >
       {
          Object.keys(moneyOrder).map((ele)=>{
            return (
            <Descriptions.Item 
              label={ele}
              key={ele}
            >
              <span className={ele==='money'?'bigger':''}>
                {moneyOrder[ele]}
              </span>
            </Descriptions.Item>
            )
          })
        }
      </Descriptions>  
    </div>
  )
  const steps = [
    {
      title: '填写转账信息',
      content:content1,
    },
    {
      title: '确认转账信息',
      content: content2,
    },
    {
      title: '完成',
      content: content3,
    },
  ];
  const next = () => {
    setCurrent(current + 1);
  };
  const prev = () => {
    setCurrent(current - 1);
  };
  return (
    <>
      <Steps current={current}>
        {steps.map(item => (
          <Step key={item.title} title={item.title} />
        ))}
      </Steps>
      <div className="steps-content">{steps[current].content}</div>
    </>
  );
};
