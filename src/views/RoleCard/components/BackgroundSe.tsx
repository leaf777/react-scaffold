import { Radio,  Menu, Dropdown, Button, message, Space, Tooltip } from 'antd'
import { CaretDownFilled } from '@ant-design/icons';
import { useState } from 'react';
import './style.scss'
import '../components/style.scss'
export default(props)=>{
  const [state,setState]=useState({value:1})
  const [color,setColor]=useState('Blue')
  props.onReceive(color)
  const onChange = e => {
    console.log('radio checked', e.target.value);
    setState({
      value: e.target.value,
    });

  };
  
  const handleMenuClick=(e)=>{
    message.info('Click on menu item.');
    setColor(e.key)
    console.log('click', e);
  }
  const menu = (
    <Menu onClick={handleMenuClick} style={{width:'200px'}}>
      <Menu.Item key="Blue" style={{display:'inline-block'}}>
        <div className='Blue color'></div> 
      </Menu.Item>
      <Menu.Item key="Glary">
        <div className='Glary color'></div> 
      </Menu.Item>
      <Menu.Item key="Green">
        <div className='Green color'></div> 
      </Menu.Item>
      <Menu.Item key="Red">
        <div className='Red color'></div> 
      </Menu.Item>
      <Menu.Item key="Yellow">
        <div className='Yellow color'></div>   
      </Menu.Item>
      <Menu.Item key="Pink">
        <div className='Pink color'></div> 
      </Menu.Item>
    </Menu>
  );
  return(
    <>
      <Radio.Group onChange={onChange} value={state.value}>
        <Space direction="vertical">
          <Radio value={1}>
            <span style={{paddingRight:'20px'}}>背景色</span>
            <Space wrap>
              <Dropdown overlay={menu} trigger={['click']}>
                <Button>
                  <div style={{height:'18px'}}>
                    <div className={`color ${color}`} style={{display:'inline-block'}}></div>
                    <CaretDownFilled style={{height:'10px',display:'inline-block'}}/>
                  </div>
                </Button>
              </Dropdown>
            </Space>
          </Radio>
          <Radio value={2} style={{paddingTop:'20px'}}>
            背景图
          </Radio>
        </Space>
      </Radio.Group>
    </>
  )

}