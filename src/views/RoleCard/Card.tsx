import {useHistory } from 'react-router-dom'
import { Tabs } from 'antd';
import loadable from '@loadable/component'
import { Route, Switch } from 'react-router-dom'
const RoleCard = loadable(()=>import('@/views/RoleCard/CardManage'))
const CardRecord = loadable(()=>import('@/views/RoleCard/CardRecord'))
const FadeRecord = loadable(()=>import('@/views/RoleCard/FadeRecord'))
const AddCard = loadable(()=>import('@/views/RoleCard/AddCard'))
const { TabPane } = Tabs;

export default()=>{
  let history = useHistory();
  function callback(key) {  
    history.push(key);
  }  
  return(
    <div>
      <Tabs defaultActiveKey="1" onChange={callback}>
        <TabPane tab="权益卡管理" key="/Card/">
        </TabPane>
        <TabPane tab="领卡记录" key="/Card/CardRecord">
        </TabPane>
        <TabPane tab="退卡记录" key="/Card/FadeRecord">
        </TabPane>
      </Tabs>
      <Switch>
        <Route key='700101' path='/Card/CardRecord' component={CardRecord} />
        <Route key='700102' path='/Card/FadeRecord' component={FadeRecord} />
        <Route key='700104' path='/Card/AddCard' component={AddCard} />
        <Route key='700103' path='/Card/' component={RoleCard} />
      </Switch>
    </div>
  )
}


// children:[
//   {
//     id:700101,
//     text:'权益卡管理',
//     path:'/',
//     permission:['editor','admin'],
//     component:RoleCard
//   },
//   {
//     id:700102,
//     text:'领卡记录',
//     path:'/CardRecord',
//     permission:['editor','admin'],
//     component:CardRecord
//   },
//   {
//     id:700103,
//     text:'退卡记录',
//     path:'/FadeRecord',
//     permission:['editor','admin'],
//     component:FadeRecord
//   }
//  ]