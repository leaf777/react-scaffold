import React ,{useState,useEffect} from 'react'
import {useHistory } from 'react-router-dom'
import { Button,Tabs } from 'antd'
import './style.scss'

const { TabPane } = Tabs;

export default  () => {
  let history = useHistory();
  const AddCard=(key)=>{
    history.push(key);
  }
 return(
   <>
    <Button 
      type="primary" 
      size='middle' 
      style={{marginBottom:'10px'}} 
      onClick={()=>(history.push('/Card/AddCard'))}
    >
      新建权益卡
    </Button>
    <div className="card-container">
      <Tabs type="card">
        <TabPane tab="Tab Title 1" key="1">
          <p>Content of Tab Pane 1</p>
          <p>Content of Tab Pane 1</p>
          <p>Content of Tab Pane 1</p>
        </TabPane>
        <TabPane tab="Tab Title 2" key="2">
          <p>Content of Tab Pane 2</p>
          <p>Content of Tab Pane 2</p>
          <p>Content of Tab Pane 2</p>
        </TabPane>
        <TabPane tab="Tab Title 3" key="3">
          <p>Content of Tab Pane 3</p>
          <p>Content of Tab Pane 3</p>
          <p>Content of Tab Pane 3</p>
        </TabPane>
      </Tabs>
    </div>
   </>
 )
};
