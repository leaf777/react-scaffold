import React from 'react'
import loadable from '@loadable/component'
import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
  FormOutlined,
  SettingFilled
} from '@ant-design/icons'

const StudyCombine = loadable(()=>import('@/views/content/StudyCombine'))
const StudyComponent = loadable(()=>import('@/views/content/StudyComponent'))
const StudyCommunication = loadable(()=>import('@/views/content/StudyCommunication'))
const StudyCondition = loadable(()=>import('@/views/content/StudyCondition'))
const Home=loadable(()=>import('@/views/home'))
const titleAddOrEditor = loadable(()=>import('@/views/title/titleAddOrEditor'))
const userManager = loadable(()=>import('@/views/user/UserManager'))
const selectTitle = loadable(()=>import('@/views/title/selectTitle'))
const mapManage = loadable(()=>import('@/views/map/Map.jsx'))
const Steps = loadable(()=>import('@/views/step/Steps'))
const txMap = loadable(()=>import('@/views/map/TXMap'))
const RoleSetting = loadable(()=>import('@/views/youzanDemo/RoleSetting'))
const AddRole = loadable(()=>import('@/views/youzanDemo/AddRole'))
const DeleteRole = loadable(()=>import('@/views/youzanDemo/DeleteRole'))
const StaffSetting = loadable(()=>import('@/views/youzanDemo/StaffSetting'))
const AddStaff = loadable(()=>import('@/views/youzanDemo/AddStaff'))
const Card = loadable(()=>import('@/views/RoleCard/Card'))
const AddCard = loadable(()=>import('@/views/RoleCard/AddCard'))
const TemplateImage = loadable(()=>import('@/views/RoleCard/components/TemplateImage'))


export const constRouter=[
  {
    id: 1,
    text: '首页',
    icon: <UploadOutlined/>,
    children: [
      {
        id: 101,
        text: '任务面板',
        path: '/',
        component: Home
      }
    ]
  },
  {
    id:2,
    text:'地图',
    icon:<UploadOutlined />,
    children:[
      {
        id:201,
        text:'城市地图',
        path:'/map',
        component:mapManage
      },
      {
        id:202,
        text:'腾讯地图',
        path:'/txmap',
        component:txMap
      }
    ]
  },
  {
    id:3,
    text:'表单页',
    icon:<FormOutlined />,
    children:[
      {
        id:301,
        text:'分布表单',
        path:'/stepForm',
        component:Steps
      }
    ]
  }
]
const asyncRoute=[
  {
    id:10,
    text:'基础知识',
    icon:<UserOutlined />,
    permission:['editor'],
    children:[
      {
        id:1001,
        text:'通信',
        path:'/communication',
        permission:['editor'],
        component:StudyCommunication
      }
    ]
  },
  {
    id:20,
    text:'进阶知识',
    icon:<VideoCameraOutlined />,
    permission:['admin'],
    children:[
      {
        id:2001,
        text:'两种组件',
        path:'/component',
        permission:['admin'],
        component:StudyComponent,
        children:[
          {
            id:200101,
            text:'学习',
            path:'/study',
            permission:['admin'],
            component:StudyComponent
          }
        ]
      },
      {
        id:2002,
        text:'高级组件',
        path:'/Combine',
        permission:['editor'],
        component:StudyCombine 
      }
    ]
  },
  {
    id:30,
    text:'查漏补缺',
    icon:<UploadOutlined />,
    permission:['editor'],
    children:[
      {
        id:3001,
        text:'Condition',
        path:'/condition',
        permission:['editor'],
        component:StudyCondition
      }
    ]
  },
  {
    id:40,
    text:'文章管理',
    icon:<VideoCameraOutlined />,
    permission:['editor'],
    children:[
      {
        id:4001,
        text:'新增文章',
        path:'/AddTitle',
        permission:['editor'],
        component:titleAddOrEditor
      },
      {
        id:4002,
        text:'修改文章',
        path:'/ChangeTitle',
        permission:['editor'],
        component:titleAddOrEditor
      },
      {
        id:4003,
        text:'查看文章',
        path:'/SelectTitle',
        permission:['editor'],
        component:selectTitle
      },
      
    ]
  },
  {
    id:50,
    text:'用户管理',
    icon:<VideoCameraOutlined />,
    permission:['admin'],
    children:[
      {
        id:5001,
        text:'查看用户',
        path:'/SelectUser',
        permission:['admin'],
        component:userManager
      }
    ]
  },
  {
    id:60,
    text:'设置',
    icon:<SettingFilled />,
    permission:['admin','editor'],
    children:[
      {
        id:6001,
        text:'角色管理',
        path:'/roleManage',
        permission:['admin','editor'],
        component:RoleSetting,
        children:[
          {
            id:600101,
            text:'添加角色',
            permission:['admin','editor'],
            path:'/roleManage/addRole',
            component:AddRole
          },
          {
            id:600102,
            text:'删除角色',
            permission:['admin','editor'],
            path:'/roleManage/deleteRole',
            component:DeleteRole
          }
        ]
      },
      {
        id:6002,
        text:'员工管理',
        permission:['admin','editor'],
        path:'/staffManage',
        component:StaffSetting,
        children:[
          {
            id:600201,
            text:'添加员工',
            permission:['admin','editor'],
            path:'/staffManage/addStaff',
            component:AddStaff
          }
        ]
      }
    ]
  },
  {
    id:70,
    text:'权益卡',
    icon:<UploadOutlined />,
    permission:['editor','admin'],
    children:[
     {
       id:7001,
       text:'权益卡',
       permission:['editor','admin'],
       path:'/Card',
       component:Card,
       children:[
         {
           id:700104,
           text:'新增权益卡',
           permission:['editor','admin'],
           path:'/Card/AddCard',
           component:AddCard,
         },
         {
           id:700105,
           text:'虚假图片处理',
           permission:['editor','admin'],
           path:'/RoleCard/TemplateImage',
           component:TemplateImage
         }
       ]
     }
    ]
  },
]
const role:any =localStorage.getItem('role')||''
const qf=localStorage.getItem('gn')?.split(',')
console.log(qf,'qf')
qf && qf.map(ele=>{
  if(ele.length>2 && ele.length<=4){
    const f2=ele.slice(0,4)
    if(!qf.includes(f2)){
      qf.push(f2)
    }
  }
  if(ele.length>4){
    const f1=ele.slice(0,2)
    const f2=ele.slice(0,4)
    if(!qf.includes(f1)){
      qf.push(f1)
    }
    if(!qf.includes(f2)){
      qf.push(f2)
    }
  }
})
console.log(qf,'qf')
asyncRoute.map((ele,idx)=>{
  if(qf && qf.includes(ele.id+'')){
    ele.permission.push(role)
    if(ele.children){
      ele.children.map((ele2,index)=>{
        if(qf && qf.includes(ele2.id+'')){
          ele2.permission.push(role)
          if(ele2.children){
            ele2.children.map((ele3,i)=>{
              if(qf && qf.includes(ele3.id+'')){
                ele3.permission.push(role)
              }
            })
          }
        }
      })
    }
  }
})
console.log('asyncRoute',asyncRoute)
export default asyncRoute
