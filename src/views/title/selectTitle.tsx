import React from 'react'
import { Breadcrumb,Row,Col,Input,DatePicker,Table, Tag, Space} from 'antd';
import { useState,useEffect } from 'react';
import { HomeOutlined, UserOutlined } from '@ant-design/icons';
import { useAppSelector,useAppDispatch } from '@/hooks';
import {selectAllTitle} from '@/store/reducers/article'

const { Search } = Input;
const { RangePicker } = DatePicker;
// const moment = require('moment')
export default ()=> {
  const dispatch = useAppDispatch()
  const [name, setName] = useState('')
  const titleList=useAppSelector(({article})=>article.titleList)
  const onSearch = value => setName(value)
  const columns = [
    {
      title: '文章名',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: '作者',
      dataIndex: 'author',
      key: 'author',
    },
    {
      title: '缩略图',
      dataIndex: 'image',
      key: 'image',
      render: image=> (
       <img  src={image} style={{width:'30px',height:'30px'}}/>
      )
    },
    {
      title: '类别',
      key: 'type',
      dataIndex: 'type',
      render: type => {
        let color = type.length > 5 ? 'geekblue' : 'green';
          return (
            <Tag color={color} key={type}>
              {type.toUpperCase()}
            </Tag>
          );
      },
    },
    {
      title: '行为',
      key: 'action',
      render: (record) => (
        <Space size="middle">
          <a>查看 {record.title}</a>
          <a>删除</a>
        </Space>
      ),
    },
  ];
  useEffect(()=>{
    dispatch(selectAllTitle({title:name}))
  },[name])
  return(
    <div className='yx-user-manager'>
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item href="">
          <UserOutlined />
          <span>查看文章</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item></Breadcrumb.Item>
      </Breadcrumb>
      <Row style={{padding:"10px 0"}}>
        <Col span={8}>
          <Search
            placeholder="请输入文章名"
            allowClear
            enterButton="Search"
            size="large"
            onSearch={onSearch}   
          />
        </Col>
        <Col span={8}></Col>
        <Col span={8} style={{lineHeight:'40px',textAlign:'center'}}>
          
        </Col>
      </Row>
      <Table columns={columns} dataSource={titleList} rowKey='_id' />
    </div>
  )

}


