import axios from '@/utils/axios.ts'

export const fetchUser=data=>axios({url:'/api/v3/user/login',method:'post',data})
export const fetchUserInfo=params=>axios({url:'/api/v3/user/info',method:'get',params})
export const fetchUserList=params=>axios({url:'/api/v3/user/list',method:'get',params})
export const fetchAddUser=data=>axios({url:'/api/v3/user/add',method:'post',data})
export const fetchCateList=params=>axios({url:'/api/v3/title/cate',method:'get',params})
export const fetchChart=params=>axios({url:'/api/v3/dash/chart',method:'get',params})
export const fetchTitleList=params=>axios({url:'/api/v3/title/titlelist',method:'get',params})
export const fetchAddTitle=data=>axios({url:'/api/v3/title/add',method:'post',data})
export const fetchEcharts=params=>axios({url:'/api/v3/dash/echart',method:'get',params})
export const fetchAddOrder=data=>axios({url:'/api/v3/step/add',method:'post',data})
export const fetchSelectOrder=params=>axios({url:'/api/v3/step/select',method:'get',params})
export const fetchChangeOrder=params=>axios({url:'/api/v3/step/change',method:'get',params})
export default {
    fetchUser,
    fetchUserInfo,
    fetchUserList,
    fetchAddUser,
    fetchCateList,
    fetchChart,
    fetchTitleList,
    fetchEcharts,
    fetchAddTitle,
    fetchAddOrder,
    fetchSelectOrder,
    fetchChangeOrder
  }  
// 这样就有两种方式导入
// import api from '@/api'
// api.fetchgoodList()
// import {fetchGoodList} from '@/api
// fetchgoodList()
