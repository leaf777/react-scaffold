import { 
  TEST_MSG_UPDATE,
  LOGIN_TOKEN_UPDATE,
  GET_TOKEN_TOKEN
 } from '../type' 

 import { fetchUser } from '@/api'

// action:是redux工作流程的核心，它由dispatch
// action生成器
export function updateMsg(payload) {
  return {
    type:TEST_MSG_UPDATE,
    payload
  }
}


// token
export function updateToken(payload) {
  return {
    type:LOGIN_TOKEN_UPDATE,
    payload
  }
}


//登陆，传递用户名和密码给后端，后端返回token,再dispatch 一个类型为LOGIN_TOKEN_UPDATE设置token

export function getToken(params) {
  return dispatch => {
    // 调接口
    fetchUser(params).then(token=>{
      console.log('后端返回的token', token)
      dispatch({
        type:LOGIN_TOKEN_UPDATE ,
        payload: token
      })
    })
  }

}