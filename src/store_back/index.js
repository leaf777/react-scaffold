// Redux的3个"三"
// 三个核心API：createStore,combineReducer,applyMiddleware
//三个概念：store、reducer、action

import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import test from './reducers/test'



export default createStore(
  combineReducers({
    test
  }),
  applyMiddleware(thunk)
)

