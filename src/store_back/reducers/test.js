import produce from 'immer'
// immutable也是深复制的
const initState={
  msg:'hello redux',
  token:'',
  list:[]
}
import { TEST_MSG_UPDATE,LOGIN_TOKEN_UPDATE } from '../type'
export default function (state=initState,{type,payload}){
  return produce(state, (newState)=>{
    switch(type) {
      case TEST_MSG_UPDATE:
        newState.msg=payload
        break
      case LOGIN_TOKEN_UPDATE:
        newState.token=payload
        break
      default:
        newState=state
    }
    return newState
  })
}
//reducer作用：用于修改store，是真正做事的，redux工厂中的员工
 
// const initState =  {
//   msg:'hello redux',
//   list:[]
// }
// //  reducer真正修改数据的地方
// function reducer(state,action){
// state=initState
// //根据action(信号)来做不同的事
// //JSON.parse(JSON.stringify())---深赋值
// const newState = JSON.parse(JSON.stringify(state))
// if(action.type==='update'){
//   console.log(action.payload)
//   newState.msg=action.payload
// }
// return newState
// }

// export default createStore(reducer)