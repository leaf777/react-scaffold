import React, { Children } from 'react'
import { useState ,useEffect} from 'react';
import { useAppDispatch ,useAppSelector} from '@/hooks';
import { Select } from 'antd';
import { selectArticleCate } from '@/store/reducers/article'


const { Option } = Select;
// 从后端获取数据

export default ({value, onChange})=> {
  const cateList = useAppSelector(state=>state.article.cateList)
  const dispatch=useAppDispatch()
  // 一进来就调后端接口获取类别
  useEffect(()=>{
    dispatch(selectArticleCate({}))
  }, [])
  return(
    <Select 
    placeholder="全部类别" 
    style={{ width: 120 }} 
    optionFilterProp="children"
    value={value}
    allowClear
    showSearch
    filterOption={(input, option:any) =>
      option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
    }
    onChange={value=>onChange&&onChange(value)}
    >
      {
        cateList.map((ele:any)=>(
          <Option key={ele._id} value={ele.cate}>{ele.cateName}</Option>
        ))
      }
    </Select>
  )
}
