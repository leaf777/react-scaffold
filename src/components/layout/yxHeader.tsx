import React from 'react'
import Breadcrum from '@/views/components/Breadcrum'

export default () => {
  return (
    <div className='yx-header'>
      <Breadcrum />
    </div>
  )
}