import React from 'react'
import { Link,useHistory } from 'react-router-dom'
import { Menu } from 'antd'
import { MenuUnfoldOutlined, MenuFoldOutlined } from '@ant-design/icons'

import routes from '@/views'
import img from '@/utils/img'
import { useAppSelector } from '@/hooks'
import { constRouter } from '@/views'

const Toggle = ({value, onChange}) => (
  <div className='toggle'>
    {
      value
      ? <MenuUnfoldOutlined onClick={()=>onChange(false)} />
      : <MenuFoldOutlined onClick={()=>onChange(true)} />
    }
  </div>
)
const { SubMenu } = Menu
export default props => {
  console.log('QfSider props', props)

  const history = useHistory()
  const skip = ()=> history.replace('/')
  const u = useAppSelector(({test})=>test.user)

  return (
    <div className='yx-sider'>
      <div className={`logo ${props.value ? "off" : ""}`}>
        <img src={img.logo} onClick={skip} />
      </div>
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={['1']}
      >
        {/* 暂时就不改层级了，在yxContent已经修改过 */}
      {
        //静态路由
        constRouter.map(ele=>{
          return <SubMenu key={ele.id} icon={ele.icon} title={ele.text}>
          {
            ele.children && ele.children.map(ele=>{
                return <Menu.Item key={ele.id}>
                <Link to={ele.path}>{ele.text}</Link>
                </Menu.Item>
              })
          }
          </SubMenu>
        })
      }
      {
        // 动态路由
        routes.map(ele=>{
          if(ele.permission && ele.permission.includes(u.role[0])){
           return <SubMenu key={ele.id} icon={ele.icon} title={ele.text}>
          {
            ele.children && ele.children.map(ele=>{
              if(ele.permission && ele.permission.includes(u.role[0])){
                return <Menu.Item key={ele.id}>
                <Link to={ele.path}>{ele.text}</Link>
                </Menu.Item>
              } 
            })
          }
          </SubMenu>
          }  
        })
      }
      </Menu>

      {/* 属性继承 */}
      <Toggle {...props} />
    </div>
  )
}