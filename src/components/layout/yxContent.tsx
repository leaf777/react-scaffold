// 根据路由匹配不同的组件，所以遍历content中的组件，将其放置路由中，并根据不同的路由在Content模块中显示不同的内容
import React,{ useCallback } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { useAppSelector } from '@/hooks'
import routes,{ constRouter } from '@/views'


export default() =>{
  const u = useAppSelector(({test})=>test.user)
  let result:any = []

  const renderRoute = useCallback(
    (arr) => {
      result=[]
      //  坑：renderMenu方法要定义在renderRout里面，否则会显示不出内容
      const renderMenu= arr =>{
        arr.map(ele=>{
        if(ele.children) renderMenu(ele.children)
        if(ele.permission && ele.permission.includes(u.role[0])){
          console.log(ele.path,'path')
          if(ele.path==='/Card'){
            result.push(
              <Route key={ele.id} path={ele.path} component={ele.component} />
            )
          }else{
            result.push(
              <Route exact key={ele.id} path={ele.path} component={ele.component} />
            )
          } 
        }     
        })
      } 
      arr.map(ele=>{
        renderMenu(ele.children)
      })
      // renderMenu(arr)
      console.log(result,'result')
      return result
    },
    []
  ) 
const renderRoute2 = useCallback(
  (arr) => {
    let result:any = []
    const recursion = arr => {
      arr.map(ele=>{
        result.push(
          <Route exact key={ele.id} path={ele.path} component={ele.component} />
        )
        if(ele.children) recursion(ele.children)
      })
    }
    arr.map(ele=>{
      recursion(ele.children)
    })
    return result
  },
  []
)
    return (
      <div className='yx-content'>
        <Switch>
          { renderRoute2(constRouter) }
          { renderRoute(routes) } 
          <Redirect from='/*' to='/' />
        </Switch>
      </div>
    )
}