import React, { useState } from 'react'

import { Layout } from 'antd'

import YxContent from './yxContent'
import YxSider from './yxSider'
import YxHeader from './yxHeader'
import YxFooter from './yxFooter'
import './style.scss'

const { Header, Sider, Content, Footer } = Layout

export default props => {
  const [collapsed, setCollapsed] = useState(false)

  return (
    <div className='yx-layout'>

      <Layout>

        <Sider
          trigger={null}
          collapsible
          collapsed={collapsed}
        >
          <YxSider value={collapsed} onChange={bol=>setCollapsed(bol)} />
        </Sider>

        <Layout>

          <Header>
            <YxHeader />
          </Header>

          <Content>
            <YxContent />
          </Content>

          <Footer>
            <YxFooter {...props} />
          </Footer>
        </Layout>
      </Layout>
    </div>
  )
}
