import React from 'react'
import { BrowserRouter} from 'react-router-dom'

import { Provider } from 'react-redux'
import store from '@/store'
import DashBoard from './dashboard'
import '@/assets/style.css'
import '@/assets/style.scss'
import 'antd/dist/antd.css';
function App() {
  return (
    
      <BrowserRouter>
         <Provider store={store}>
         <DashBoard />
         </Provider>
       </BrowserRouter> 
  );
}

export default App;
