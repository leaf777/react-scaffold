// Redux的3个"三"
// 三个核心API：createStore,combineReducer,applyMiddleware
//三个概念：store、reducer、action
import { configureStore } from '@reduxjs/toolkit'

import thunk from 'redux-thunk'
import test from '@/store/reducers/test'
import user from '@/store/reducers/user'
import article from '@/store/reducers/article'
import chart from '@/store/reducers/chart'
import step from '@/store/reducers/step'
import myroute from '@/store/reducers/myroute'

const store = configureStore({
  reducer: {
    test,
    user,
    article,
    chart,
    step,
    myroute
  },
  middleware: getDefaultMiddleware=>[...getDefaultMiddleware(), thunk]
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export default store


