import { 
  TEST_MSG_UPDATE,
  LOGIN_TOKEN_UPDATE,
  GET_USER_INFO
 } from '../type' 

 import { fetchUser,fetchUserInfo } from '@/api'
 import { useAppDispatch } from '@/hooks'

// action:是redux工作流程的核心，它由dispatch
// action生成器
export function updateMsg(payload) {
  return {
    type:TEST_MSG_UPDATE,
    payload
  }
}



// 获取用户信息
export function getInfo(token) {
  // 调接口
  // params是token
  return dispatch => {
  fetchUserInfo({token}).then(res=>{
    console.log('后端返回的用户信息',res)
    //用户的权限是数组形式，可能存有多种权限，但仅仅只对第一个权限进行了处理，有时间可以处理一下多个权限
    const user={role:res.roles,nikeName:res.nike_name}
    dispatch ({
      type:GET_USER_INFO,
      payload:user
  })
})
}
}

//登陆，传递用户名和密码给后端，后端返回token,再dispatch 一个类型为LOGIN_TOKEN_UPDATE设置token
// let dispatch=useAppDispatch()
export function getToken(params) {
  return dispatch => {
    // 调接口
    fetchUser(params).then(({token})=>{
      console.log('后端返回的token', token)
      localStorage.setItem('token',token )
      console.log(dispatch,'dispatch')
      dispatch({
        type:LOGIN_TOKEN_UPDATE ,
        payload: token
      })
    })
  }
}