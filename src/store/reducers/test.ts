import produce from 'immer'
// immutable也是深复制的
const initState={
  msg:'hello redux',
  token:localStorage.getItem('token'),
  user:{
    role:'',
    nikeName:''
  },
}
type actionType={
  type:string,
  payload ?:any
}

import { TEST_MSG_UPDATE,LOGIN_TOKEN_UPDATE,GET_USER_INFO } from '../type'
export default function (state=initState,{type,payload}:actionType){
  return produce(state, (newState)=>{
    switch(type) {
      case TEST_MSG_UPDATE:
        newState.msg=payload
        break
      case LOGIN_TOKEN_UPDATE:
        newState.token=payload
        break
      case GET_USER_INFO:
        newState.user=payload
        break
      default:
        newState=state
    }
    return newState
  })
}

