// 采用新写法
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import dynRoutes,{ constRouter } from '@/views'
import dashboard from '@/dashboard'
const initialState = {
  myroute:[]
}
const getSelect = (role)=>{
  var data:Array<any>=dynRoutes.map((ele,idx)=>{
    let arr:any={}
    if(ele.permission.includes(role)||role==='all'){
      arr['title']=ele.text
      // arr['key']='0-'+idx 
      arr['key']=ele.id
      if(ele.children){
        let arr2:any=[]
        ele.children.map((ele2,index)=>{
          if(ele2.permission.includes(role)||role==='all'){
            // arr2.push({'title':ele2.text,key:'0-'+idx+'-'+index})
            arr2.push({'title':ele2.text,key:ele2.id})
            let arr3:any=[]
            if(ele2.children){
              ele2.children.map((ele3,i)=>{
                if(ele3.permission.includes(role)||role==='all'){
                // arr3.push({'title':ele3.text,key:'0-'+idx+'-'+index+'-'+i})
                arr3.push({'title':ele3.text,key:ele3.id})
                }
              })
              arr2[index]['children']=JSON.parse(JSON.stringify(arr3)) 
            }
          }
        })
        arr['children']=JSON.parse(JSON.stringify(arr2))
      }
    }
    arr=JSON.parse(JSON.stringify(arr))
    return arr
  })
  for (let i = 0; i < data.length; i++) {
    if (JSON.stringify(data[i]) == "{}") {
      data.splice(i, 1);
      console.log(data);
      i = i - 1;
    }
  }
  console.log('newArr',data)
  return data
}
const selectText = createAsyncThunk(
  'myroute/changeOrder',
  // 本该调接口去获取动态路由，但是先使用固定的把
  async() =>{
    console.log('jinlaile')
    const res:any = await getSelect('admin')
    console.log(res,'res3333333333333')
    return res
  }

)
const counterSlice = createSlice({
  name: 'myroute',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
    .addCase(selectText.fulfilled,(state,action)=>{
      console.log(action.payload,'00000000000')
      state.myroute=action.payload
    })
  }
})
export default counterSlice.reducer
export {selectText }