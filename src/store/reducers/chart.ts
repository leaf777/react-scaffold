// 采用新写法
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import {fetchChart ,fetchEcharts} from '@/api'


const initialState = {
  chart: [],
  echart:[]
}


const getChartInfo = createAsyncThunk(
  'chart/getChart',
  async(data:any) => {
    const res=await fetchChart(data)
    return res
  }
)
const getEchartInfo = createAsyncThunk(
  'chart/getEchartInfo',
  async(data:any) =>{
    const res=await fetchEcharts(data)
    return res
  }
)
const counterSlice = createSlice({
  name: 'chart',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
    .addCase(getChartInfo.fulfilled, (state,action) => {
      state.chart=action.payload
      console.log('添加成功')
    })
    .addCase(getEchartInfo.fulfilled,(state,action)=>{
      state.echart = action.payload
      console.log('添加成功了')
    })
  }
})
export default counterSlice.reducer
export {getChartInfo,getEchartInfo}