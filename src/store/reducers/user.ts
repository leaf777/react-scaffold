// 采用新写法
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import {fetchUserList,fetchAddUser } from '@/api'


const initialState = {
  user: {},
  total: 0,
  list: []
}
// 根据用户名查找用户信息，默认用户名为空查找
const selectUser = createAsyncThunk(
  'user/selectUser',
  // Declare the type your function argument here:
  async (params:any) => {
    const res = await fetchUserList(params)
    // Inferred return type: Promise<MyData>
    console.log(res,'搜素用户名的返回值')
    return res
  }
)
// 增加用户信息
const addUser = createAsyncThunk(
  'user/addUser',
  async(data:any) => {
    const res=await fetchAddUser(data)
  }
)
const counterSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
    .addCase(selectUser.fulfilled, (state, action) => {
      state.list = action.payload.list
      state.total=action.payload.total.length
    })
    .addCase(addUser.fulfilled, () => {
      console.log('添加成功')
    })
  }
})

export default counterSlice.reducer
export {selectUser ,addUser}