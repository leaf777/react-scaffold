// 采用新写法
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import {fetchAddOrder, fetchSelectOrder,fetchChangeOrder} from '@/api'

const initialState = {
  moneyOrder:{},
  orderId:''
}
const addOrder = createAsyncThunk(
  'step/addOrder',
  async(data:any) => {
    const res=await fetchAddOrder(data)
    localStorage.setItem('orderId',res.orderId )
    return res
  }
)
const selectOrder = createAsyncThunk(
  'step/selectOrder',
  async(params:any) =>{
    const res =await fetchSelectOrder(params)
    return res
  }
)

const changeOrder = createAsyncThunk(
  'step/changeOrder',
  async(params:any) =>{
    const res = await fetchChangeOrder(params)
    return res
  }
)
const counterSlice = createSlice({
  name: 'step',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
    .addCase(addOrder.fulfilled,(state, action)  => {
      state.orderId=action.payload.orderId || ''
    })
    .addCase(selectOrder.fulfilled,(state,action)=>{
      state.moneyOrder = action.payload.order
    })
    .addCase(changeOrder.fulfilled,(state)=>{
    })
  }
})
export default counterSlice.reducer
export {addOrder,selectOrder,changeOrder}