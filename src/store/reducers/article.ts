import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import {fetchCateList ,fetchTitleList,fetchAddTitle} from '@/api'


const initialState = {
  cateList: [],
  titleList:[]
}
// 根据用户名查找用户信息，默认用户名为空查找
const selectArticleCate = createAsyncThunk(
  'article/selectArticleCate',
  async (params:any) => {
    const res = await fetchCateList(params)
    console.log(res,'文章类别的返回值')
    return res
  }
)
const selectAllTitle = createAsyncThunk(
  'article/selectAllTitle',
  async(params:any)=>{
    const res = await fetchTitleList(params)
    return res
  }
)
const addTitle = createAsyncThunk(
  'article/addTitle',
  async(data:any)=>{
    const res = await fetchAddTitle(data)
    return res
  }
)
const counterSlice = createSlice({
  name: 'article',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
    .addCase(selectArticleCate.fulfilled, (state, action) => {
      state.cateList = action.payload.data
    })
    .addCase(selectAllTitle.fulfilled, (state, action) =>{
      state.titleList = action.payload.list
    })
    .addCase(addTitle.fulfilled,()=>{
      console.log('添加成功')
    })
  }
})

export default counterSlice.reducer
export {selectArticleCate,selectAllTitle,addTitle }