# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## 安装
- create-react-app
## 创建
- create-react-app my-app --template typescript

## 环境的二次封装

- react-dev-utils 是react官方提供的工具包



# 将环境配置分为开发环境和生产环境
- 在webpack.config.js的570行plugin配置后面将自己的plugin合并进去
  
# 默认端口修改
- 在自己的环境中设置端口，
# 代理修改

# 配置别名
- 在webpack.config.js的330行后将自己的alians合并进去

# ReactReduxContext是Provider和Consumer的结合体 


# 订阅的返回值调用就是取消订阅
 
# 自定义connect高阶组件
- 使用react-redux中的useSelector()
- store.subscribe()订阅监听store变化
- 问题：能否使用上下文
面试题
1. 封装高阶组件
2. 不同的语法


- redux没有依赖收集，mobx和vuex都需要依赖收集
- 封装useSelector,useDispatch 

- redux--数据的不可变性，JS变量可变换的

- reducer作用：用于修改state，是真正做事的，可以理解是Redux工厂中的员工。
- reducer是纯函数（不能修改入参，并且相同的入参永远得到唯一的输出）


- 装饰器：@babel/plugin-syntax-decorators

## 深复制
- const newState=JSON.parse(JSON.)  //不能复制方法
- const newState={...state}   //层级较浅
- const newState=Object.assign({},state) 
提高效率
- immutable库，
- immer库


- flux数据流思想：一切外部数据都要从props进来
- 异步调接口---按照流程的话需要两次dispatch，其后一个的返回需要返回一个没有任何语意的type，如果不想要这样做，可以通过插件解决 redux-thunk ，
- redux-thunk 要求异步action creator返回一个函数，redux-thunk会给函数dispatch参数
在异步的进入之前，判断是否是function，如果是function则在回调函数中加入patch
如果不是就跳过

## 关于登陆和首页
- 登陆和首页两者只能存在一个，根据token来判断是登陆界面还是首页界面，用来判断的token存储在状态管理中，当用户登陆成功后，将token存储到本地存储中，并用本地存储中的值来改变token，
- 本地存储中的值来改变token，这一点特别重要，因为状态管理中的值每次刷新后就没有了，用本地存储的token值来设置的话，当本地存储中的token失效时，状态管理就获取不到token，所以用户就得重新登陆。


# 简单描述Redux传统流程：

- 安装redux，使用createStore(rootReducer、initState、middleWares)创建根store
- 使用combineReducers({}) 合并多个子reducer
- 使用compose(applyMiddleWare(thunk), applyMiddleWare(logger))使用中间件，尤其是解决redux不支持异步action的问题。
- 如何创建子reducer呢？function reducer(initState,aciton) {本质上是一堆switch语句}，在编写子reducer时，核心逻辑是深复制（immer），根据不同的case分支来修改子store（action={type,payload}是信号，像一封邮件）
- 在App.jsx中，安装react-redux这个库，使用 ，React组件树中有了store上下文。
- 在React组件中，如果是16.8以前，只能使用 connect(mapState, mapDispatch)(Component)，然后在- - props上就能访问这些store数据、以及那些修改store的action逻辑。
- 在React组件中，如果是16.8以后，除了connect可以用，建议使用更好的 useDispatch、useSelector。
- 在传统的redux架构中，为了让action更好地维护和复用，我们一般建议封装action生成器方法。
- 在传统的redux架构中，为了避免协同开发时大家滥用type或者type冲突，我们一般建议封装一个type字典。

# 简单@reduxjs/toolkit的流程：

- 安装@reduxjs/toolkit这个库，使用configureStore({reducer,middleware})创建store。
- 使用createSlice({name,initialState,reducers,extraReducers})创建子reducer，最后抛出子reducer给根store合并。
- 使用createAsyncThunk('user/login', async (入参)=>(await fetchLogin()))，给到createSlice.extraReducers中addCase添加异步成功状态，在成功状态中直接修改子store。这些由createAsyncThunk创建action方法，也要抛出，给React组件进行触发使用 dispatch(login(入参))。
- 在App中，安装react-redux，使用注入上下文。
- 在React组件中，只能使用 @reduxjs/toolkit官方推荐的 useAppSelector来使用store数据、只能使用useAppDispatch来触那些子store中抛出的action。


## 权限设计
- 在设计路由的时候，加入permission属性，表示可以访问该模块的用户角色
- 当用户登陆后，在生成侧边栏和内容的模块中对路由进行筛选，静态路由直接遍历加入到<Router>中，动态路由遍历并判断当前用户的角色名是否存在其permission中，如果存在则加入<Router>中，如果不存在则说用该用户没有权限查看该模块则不显示。

## 新增管理员身份
- 当选择好新增管理员所能操作的模块提交给后端（提交的是模块唯一的key值），后端会新建管理员身份，并将数组转换成字符串存储在后端role表中，当用户登陆时，先判断用户名密码是否正确，如果用户名密码正确的话会生成token返回给前端，前端拿到token之后，根据token获取到管理员信息和本角色所能看到的模块ID（后端返回的是一个string）分割成数组并对路由添加permission权限，如果管理员被允许看这个模块，就将该管理员的role名加入到permission中，其余界面都是根据这个路由信息表进行渲染的，