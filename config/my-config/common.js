const path = require('path')
const webpack = require('webpack')


module.exports = {
  modules: ['node_modules'],
  extensions: [ '.js', '.jsx','.ts', '.tsx'],
  alias: {
    '@':path.resolve(__dirname,'../../src')
  },
  plugin:[
    new webpack.ProgressPlugin()
  ]
}