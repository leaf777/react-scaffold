module.exports={
  devServer: {
    port: 9000,
    proxy: {
      '/api': {
        target: 'http://localhost:9999',
        changeOrigin: true
      }
    }
  },
  module: {
    rules:[

    ]
  },
  plugins:[]

}