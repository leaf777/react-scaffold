const paths = require('./config/paths')
//模仿vue脚手架对create-react-app环境进行二次封装,主要是对start环境进行修改
const getClientEnvironment = require('./config/env');
const env = getClientEnvironment(paths.publicUrlOrPath.slice(0, -1));
const { merge } = require('webpack-merge')

const common = require('./config/my-config/common')
const serve = require('./config/my-config/serve')
const build = require('./config/my-config/build')

module.exports = merge(common,env.raw.NODE_ENV==='development'?serve:build)